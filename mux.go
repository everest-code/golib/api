package api

import (
	"github.com/gorilla/mux"
	"net/http"
	"regexp"
)

type (
	RouteTree struct {
		Path     string
		Handler  map[string]http.HandlerFunc
		SubPaths []RouteTree
	}
)

// SerializeRoutes use a *mux.Router and serialize all routes from a RouteTree
func SerializeRoutes(router *mux.Router, routes *RouteTree) {
	for method, route := range routes.Handler {
		router.HandleFunc(routes.Path, route).Methods(method)
	}

	for _, subPath := range routes.SubPaths {
		subPath.Path = string(regexp.MustCompile("/{2,}").ReplaceAll([]byte(routes.Path+subPath.Path), []byte("/")))
		SerializeRoutes(router, &subPath)
	}
}
